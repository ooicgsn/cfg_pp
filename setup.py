from setuptools import setup


def readme():
    with open('README.md') as f:
        return f.read()

setup(name='cgsn_run_pp',
      version='0.2.0',
      description=(
        """This module is designed for running cgsn parsers and processors
        based on simple configuration files (yaml) and information
        from platform configuration files."""
      ),
      long_description=readme(),
      classifiers=[
          'Development Status :: 3 - Alpha',
          'License :: OSI Approved :: MIT License',
          'Programming Language :: Python :: 3.5',
          'Topic :: Data Parsing :: JSON :: Scientific :: OOI',
      ],
      keywords='OOI raw data parsing and processing',
      url='https://bitbucket.org/ooicgsn/cfg_pp',
      author='Joe Futrelle',
      author_email='jfutrelle@whoi.edu',
      license='MIT',
      packages=['cgsn_run_pp'],
      install_requires=[
          'PyYaml',
      ],
      include_package_data=True,
      zip_safe=False)
