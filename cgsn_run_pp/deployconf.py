import os
import yaml

from .platconf import base_inst
from .constants import CALIBRATION_URL_TEMPLATE, DEPLOY_CONFIG_ROOT

HAS_SERIAL_NUMBER = ['spkir']

def get_deploy_config(mooring_id, deploy):
    path = os.path.join(DEPLOY_CONFIG_ROOT, mooring_id, '{}.yml'.format(mooring_id))
    return DeploymentConfig(path, deploy)

class DeploymentConfig(object):
    def __init__(self, config_file, deploy):
        if not os.path.exists(config_file):
            self.config = {}
        else:
            with open(config_file) as fin:
                y = yaml.load(fin)
            try:
                self.config = y[deploy]
            except KeyError:
                self.config = {}
        self.calib = self.config.get('calibration',{})
        self.serial = self.config.get('serial_number',{})
        self.bin_size = self.config.get('bin_size',{})
    def has_calibration(self, inst):
        return inst in self.calib
    def get_calibration_url(self, inst):
        return CALIBRATION_URL_TEMPLATE.format(self.calib[inst])
    def has_serial_number(self, inst):
        return inst in self.serial or base_inst(inst) in HAS_SERIAL_NUMBER
    def get_serial_number(self, inst):
        return self.serial.get(inst, 0) # 0 is placeholder for spkir
    def has_bin_size(self, inst):
        return inst in self.bin_size
    def get_bin_size(self, inst):
        return self.bin_size.get(inst)
    def get_depth(self, entity, default=None):
        if not 'depth' in self.config:
            return default
        return self.config['depth'].get(entity, default)
    def get_lat_lon(self):
        return self.config.get('lat'), self.config.get('lon')
