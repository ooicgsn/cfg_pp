import os
import re

from .constants import RAW_DATA_ROOT, PARSED_DATA_ROOT, PROCESSED_DATA_ROOT

"""
concepts:
- instruments ("insts") are things listed in Platform.*.inst_cfg, also *.*.inst
- hotels are things listed in Platform.*.hotel_cfg
[I realize that probably nobody else calls them "hotels"]
- insts and hotels have basenames. the basename removes any trailing number
from the name (e.g., dosta2 -> dosta). exceptions include 3dmgx3 and fb250.
- there are also imm insts, they're treated somewhat differently
- subassemblies (subassy) are listed in Platform.*.inst_cfg, they are things
like buoy, nsif, and mfn
"""

BASE_INST_EXCEPTIONS = ['3dmgx3', 'fb250']

def base_inst(name):
    """if name is something like metbk2, return 'metbk',
    but don't break for insts like 3dmgx3"""
    if name in BASE_INST_EXCEPTIONS:
        return name
    return re.sub(r'[\d]+$','',name)

def base_hotel(name):
    """if name is something like dcl36 return 'dcl',
    but pass other names through"""
    if name[:3] in ['dcl', 'cpm', 'stc']: # rda?
        return re.sub(r'[\d]+$','',name)
    else:
        return name

def list_deployments(platform):
    platform_dir = os.path.join(RAW_DATA_ROOT, platform)
    for d in sorted(os.listdir(platform_dir)):
        if re.match(r'D[0-9]+',d):
            yield d

def latest_two_deployments(platform):
    deps = list(list_deployments(platform))
    return deps[-2:]

def find_platconf(platform, deploy=None):
    """locate platform config file based on platform id
    and deployment id"""
    platform_dir = os.path.join(RAW_DATA_ROOT, platform)
    surface_cfg_suffix = 'cg_data/cfg_files/platform.cfg'
    profiler_cfg_suffix = 'cfg_files/platform.cfg'
    for cfg_suffix in [surface_cfg_suffix, profiler_cfg_suffix]:
        if deploy is not None:
            path = os.path.join(platform_dir, deploy, cfg_suffix)
            if os.path.exists(path):
                return path
    # find latest deployment
    for d in sorted(os.listdir(platform_dir)):
        if re.match(r'D[0-9]+',d):
            deploy = d
    if deploy is not None:
        return find_platconf(platform, deploy)
    raise ValueError('platform config file not found')

def parse_platconf(cfg_path):
    """parse a platform config file into key/value pairs and
    return a dict of them"""
    config = {}
    for line in open(cfg_path).readlines():
        line = re.sub(r'#.*','',line.rstrip()) # remove comments
        m = re.match(r'([^\s]+)\s*=\s*([^\s]+)\s*',line)
        if m:
            key, value = m.groups()
            config[key] = value
    return config

def wildcard(expr):
    """utility fn for wildcard matching of platconf keys
    e.g., 'dcl11.*.metbk'"""
    expr = '^{}$'.format(expr)
    wc = re.sub(r'\.',r'\.',expr)
    wc = re.sub(r'\*',r'[^.]+',wc)
    return wc

def split_key(key):
    """split a platconf key by '.'"""
    return re.split(r'\.',key)

def split_value(value):
    """split a platconf value by ','"""
    return re.split(r'\s*,\s*',value)

class PlatformConfig(object):
    """represents a platform config file"""
    def __init__(self, platform, deploy=None):
        path = find_platconf(platform, deploy)
        config = parse_platconf(path)
        self._config = config
        # extract global properties
        self.platform = self.get('Platform.id')
        self.platform_type = self.get('Platform.type')
        self.deploy = self.get('Deploy.id')
        self.lat = float(self.get('Deploy.lat',0))
        self.lon = float(self.get('Deploy.lon',0))
        # construct non-instrument-specific directories
        pd = os.path.join(self.platform, self.deploy)
        self.raw_dir = os.path.join(RAW_DATA_ROOT, pd)
        self.parsed_dir = os.path.join(PARSED_DATA_ROOT, pd)
        self.processed_dir = os.path.join(PROCESSED_DATA_ROOT, pd)
    def keys(self):
        """return all platconf keys"""
        return self._config.keys()
    def get(self, key, default=None):
        """get the raw value for a platconf key"""
        return self._config.get(key, default)
    def get_values(self, key, default=None):
        """split the raw value for the key by comma"""
        raw_v = self.get(key)
        return split_value(raw_v)
    def match(self, key_expr):
        """match a key expr. those look like keys, but
        can contain wildcards, e.g., Platform.*.inst_cfg"""
        results = {}
        regex = wildcard(key_expr)
        for key in self._config.keys():
            if re.match(regex, key):
                results[key] = self.get(key)
        return results
    def insts(self):
        """list all insts (values of *.*.inst)"""
        i = []
        m = self.match('*.*.inst')
        for v in sorted(list(m.values())):
            i.append(v)
        return i
    def inst_dcl(self, inst):
        """match an inst to its dcl (if appropriate)"""
        m = self.match('*.*.inst')
        for k, v in m.items():
            if v == inst:
                dcl = split_key(k)[0]
                return dcl
    def inst_subassy(self, inst):
        """match an inst to its subassembly"""
        m = self.match('Platform.*.inst_cfg')
        for k, v in m.items():
            if inst in split_value(v):
                subassy = split_key(k)[1]
                return subassy
            else:
                dcl = self.inst_dcl(inst)
                if dcl:
                    subassy = self.hotel_subassy(dcl)
                    return subassy
    def _hotel_subassy(self):
        hotel2subassy = {}
        for k, v in self.match('Platform.*.hotel_cfg').items():
            subassy = split_key(k)[1]
            for hotel in split_value(v):
                hotel2subassy[hotel] = subassy
        return hotel2subassy
    def hotel_subassy(self, hotel):
        """match a hotel to its subassembly"""
        return self._hotel_subassy()[hotel]
    def subassy_insts(self, subassy):
        key = 'Platform.{}.inst_cfg'.format(subassy)
        inst_list = self.match(key)[key]
        return re.split(',', inst_list)
    def hotels(self, b_hotel=None):
        """return all hotels (optionally constrained by basename)
        for example hotels('cpm') will return all cpms"""
        h = []
        for hotel in self._hotel_subassy():
            if b_hotel is None:
                h.append(hotel)
            elif b_hotel == base_hotel(hotel):
                h.append(hotel)
        return h
    def inst_ctd(self, inst):
        """return colocated ctd or metbk instrument for a given instrument e.g., flort,
        coloated means on the same subassy"""
        siblings = self.subassy_insts(self.inst_subassy(inst))
        for s in siblings:
            if s.startswith('ctd'):
                return s
        for s in siblings:
            if s.startswith('metbk'):
                return s
    def _imm_inst_dirs(self):
        """values are {port}:{inst}:{dir}"""
        raw_values = self._config.get('Platform.imm.inst_cfg')
        if raw_values is None:
            return {}
        values = re.split(r',',raw_values)
        imm_inst_dirs = {}
        for v in values:
            _, inst, dr = re.split(r':',v)
            imm_inst_dirs[inst] = dr
        return imm_inst_dirs
    def imm_insts(self):
        """insts on the imm"""
        return list(self._imm_inst_dirs())
    def imm_inst_dir(self, inst):
        """get the dir for an imm inst"""
        return self._imm_inst_dirs().get(inst, inst)
    def subassys(self):
        """return a list of all subassemblies"""
        sas = []
        for k in self.match('Platform.*.hotel_cfg'):
            sas.append(split_key(k)[1])
        return sas
