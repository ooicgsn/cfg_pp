import os
import importlib
import pkgutil
import glob
import types
import re

# filesystem utils

def safe_makedirs(path):
    """make dirs if they don't exist"""
    try:
        os.makedirs(path)
    except:
        pass

def readlines(path):
    """return the lines in a file"""
    with open(path) as fin:
        return [l.rstrip() for l in fin.readlines()]
    
def tail(path, amt=-1):
    """with a negative amt, return last n lines of a file,
    with a positive, return lines starting from n"""
    return readlines(path)[amt:]

def writelines(outpath, lines):
    """write lines to a file"""
    with open(outpath,'w') as fout:
        for line in lines:
            print(line, file=fout)

def touch(outpath):
    with open(outpath,'w') as fout:
        pass

def newer_than(a, b):
    """given two files a and b,
    return if a is modified more recently than b.
    if b does not exist, return True.
    not threadsafe"""
    if not os.path.exists(b):
        return True
    a_stat = os.stat(a)
    b_stat = os.stat(b)
    return a_stat.st_mtime > b_stat.st_mtime

def file_glob(dr, name_glob):
    return sorted(glob.glob(os.path.join(dr, name_glob)))

def change_extension(path, new_extension):
    """return a path with the extension changed.
    param extension should not include a '.' """
    dr, basename = os.path.split(path)
    bn_prefix, ext = os.path.splitext(basename)
    return os.path.join(dr, '{}.{}'.format(bn_prefix, new_extension))

# templating utils

def sub_glob(in_glob, out_glob, in_path, regex=r'\\w+'):
    """given in_glob /foo/bar/*.blah and out_glob
    "/baz/quux/*.fnord, and in_path "/foo/bar/baz.blah",
    return "/foo/bar/baz.fnord" """
    capturing = re.sub(r'\*', '({})'.format(regex), in_glob)
    captured = re.match(capturing, in_path).group(1)
    return re.sub(r'\*', captured, out_glob)

def interpolate_template(template, context):
    """assumes template is a dict, interpolate all values
    against the context"""
    result = {}
    for k, v in template.items():
        result[k] = v.format(**context)
    return result

# introspection utils

def module_exists(module_name):
    """t/f based on ImportError"""
    try:
        importlib.import_module(module_name)
        return True
    except ImportError:
        return False

def list_submodules(module_name):
    mod = importlib.import_module(module_name)
    mod_names = []
    for _, mod_name, _ in pkgutil.iter_modules(mod.__path__):
        mod_names.append(mod_name)
    return mod_names

def get_function(module_name, function_name):
    mod = importlib.import_module(module_name)
    fn = mod.__dict__[function_name]
    if not isinstance(fn, types.FunctionType):
        raise ValueError('{} is not a function'.format(function_name))
    return fn

