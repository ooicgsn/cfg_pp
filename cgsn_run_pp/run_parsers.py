import os
import re
from glob import glob
import subprocess
import traceback

from .utils import (safe_makedirs, newer_than, readlines, writelines, tail,
                   get_function, change_extension, sub_glob, interpolate_template)

from .constants import SURFACE_MOORING, PROFILER_MOORING, GLOBAL_MOORING
from .constants import PYTHON, GLOBAL_DEBUG

from .platconf import PlatformConfig, base_inst, base_hotel, latest_two_deployments
from .parsers import list_parsers, parser_exists, parser_module

import logging
from .constants import LOGGING_FORMAT
logging.basicConfig(format=LOGGING_FORMAT, level=logging.DEBUG)

MMP_UNPACK='/home/ooiuser/bin/cg_util/mmp_unpack_64bit'

# templates that control parsing

def make_template(parser, in_glob, out_glob):
    template = {
        'parser': parser,
        'in_glob': in_glob,
        'out_glob': out_glob
    }
    return template

INST_TEMPLATE = make_template(
    '{parser}',
    'cg_data/{dcl}/{name}/*.{name}.log',
    '{subassy}/{basename}/*.{name}.json'
)

HOTEL_SUPERV_TEMPLATE = make_template(
    'superv_{basename}',
    'cg_data/{name}/superv/*.superv.log',
    '{subassy}/superv/{name}/*.superv.json'
)

SYSLOG_CPM_SUPERV_TEMPLATE = make_template(
    'syslog_cpm_sprv',
    'cg_data/{cpm_path}syslog/*.syslog.log',
    '{subassy}/superv/{cpm}/*.syslog.json'
)

HOTEL_SYSLOG_TEMPLATE = make_template(
    'syslog_{basename}',
    'cg_data/syslog/*.syslog.log',
    'buoy/{basename}/*.syslog.json'
)

TOPLEVEL_TEMPLATE = make_template(
    '{name}',
    'cg_data/{name}/*.{name}.log',
    'buoy/{name}/*.{name}.json'
)

# FIXME what should the output dir be for IMM instruments?

IMM_TEMPLATE = make_template(
    'imm_{basename}',
    'cg_data/{dcl}/imm/{name}/{name}_*.DAT',
    'imm/{basename}/{name}_*.json'
)

PM_HOTEL_SYSLOG_TEMPLATE = make_template(
    'syslog_{basename}',
    'syslog/*.syslog.log',
    'buoy/{basename}/*.syslog.json'
)

PM_SYSLOG_SUPERV_TEMPLATE = make_template(
    'syslog_cpm_sprv',
    'syslog/*.syslog.log',
    'buoy/superv/*.syslog.json'
)

# special cases

EXCLUDED_SYSLOG_PARSERS = ['syslog_cpm_sprv'] # this is just for profilers

# map inst basename to parser name for special cases
INST_PARSER = {
    'hyd': 'hydgn',
    '3dmgx3': 'mopak',
}

def inst_parser(inst_basename):
    return INST_PARSER.get(inst_basename, inst_basename)

def render_template(platconf, template, context):
    """render a template and return its three parts:
    parser, in glob, and out glob. fully qualify
    those three parts, which are given as relative
    names in the template"""
    i = interpolate_template(template, context)
    i['parser'] = parser_module(i['parser'])
    i['in_glob'] = os.path.join(platconf.raw_dir, i['in_glob'])
    i['out_glob'] = os.path.join(platconf.parsed_dir, i['out_glob'])
    return i

def render_surface_templates(config, run_inst=None):
    """param config: PlatformConfig"""
    if config.platform_type != SURFACE_MOORING:
        raise ValueError('not a surface mooring')
    # first, insts
    for inst in config.insts():
        if run_inst is not None and not inst.startswith(run_inst):
            continue
        inst_basename = base_inst(inst)
        parser_basename = inst_parser(inst_basename)
        yield render_template(config, INST_TEMPLATE, {
            'name': inst,
            'basename': parser_basename, # FIXME is this right?
            'parser': parser_basename,
            'dcl': config.inst_dcl(inst),
            'subassy': config.inst_subassy(inst)
        })
    # next, superv associated with dcl/cpms
    for hotel in config.hotels('dcl') + config.hotels('cpm'):
        hotel_basename = base_hotel(hotel)
        subassy = config.hotel_subassy(hotel)
        yield render_template(config, HOTEL_SUPERV_TEMPLATE, {
            'name': hotel,
            'basename': hotel_basename,
            'subassy': subassy
        })
    # do syslog cpm superv
    for cpm in config.hotels('cpm'):
        subassy = config.hotel_subassy(cpm)
        if cpm == 'cpm1':
            cpm_path = '/'
        else:
            cpm_path = '{}/'.format(cpm)
        yield render_template(config, SYSLOG_CPM_SUPERV_TEMPLATE, {
            'cpm': cpm,
            'cpm_path': cpm_path,
            'subassy': subassy
        })
    # for syslog, we work backwards from parsers
    for parser in list_parsers():
        if not re.match(r'syslog_', parser) or parser in EXCLUDED_SYSLOG_PARSERS:
            continue
        yield render_template(config, HOTEL_SYSLOG_TEMPLATE, {
            'basename': re.sub(r'syslog_','',parser)
        })
    # and handle any toplevel instruments
    for name in os.listdir(os.path.join(config.raw_dir, 'cg_data')):
        yield render_template(config, TOPLEVEL_TEMPLATE, {
            'name': name
        })

def render_profiler_templates(config):
    if config.platform_type != PROFILER_MOORING:
        raise ValueError('not a profiler mooring')
    # for syslog, work backwards from parsers
    for parser in list_parsers():
        if not re.match(r'syslog_', parser) or parser in EXCLUDED_SYSLOG_PARSERS:
            continue
        yield render_template(config, PM_HOTEL_SYSLOG_TEMPLATE, {
            'basename': re.sub(r'syslog_','',parser)
        }) # a priori, parser exists
    # except for superv, that is a special case
    yield render_template(config, PM_SYSLOG_SUPERV_TEMPLATE, {})
    # mmp is handled specially, not with templates

def execute_command(cmd, **kw):
    logging.info('running {}'.format(cmd))
    try:
        subprocess.run(cmd, **kw)
        return True
    except CalledProcessError:
        traceback.print_exc()
        return False
    except TimeoutExpired:
        traceback.print_exc()
        return False

def execute_parser(parser_module, args):
    main = get_function(parser_module, 'main')
    logging.info('running {}.main({})'.format(parser_module, args))
    try:
        main(args)
        return True
    except KeyboardInterrupt:
        raise
    except:
        import traceback
        traceback.print_exc()
        return False
    
def execute_template(template):
    """given a template, run the parser.
    if the parser doesn't exist, return None"""
    parser = template['parser']
    if not parser_exists(parser):
        logging.debug('no parser called {}, this is ok'.format(parser))
        return True
    in_glob = template['in_glob']
    out_glob = template['out_glob']
    # iterate over input files
    for fin in reversed(sorted(glob(in_glob))):
        # if in glob ends *.flort.log and out glob ends *.flort.json,
        # we want to change 20170903.flort.log to 20170903.flort.json,
        fout = sub_glob(in_glob, out_glob, fin)
        if newer_than(fin, fout):
            # make sure output directory exists
            safe_makedirs(os.path.dirname(fout))
            args = ['-i', fin, '-o', fout]
            # FIXME hardcoded
            if parser == 'cgsn_parsers.parsers.parse_ctdbp':
                args += ['-s', '1'] # how to configure? this is not always 1
            # end hardcoded
            status = execute_parser(parser, args)
            if GLOBAL_DEBUG:
                if status and os.path.exists(fout) and os.path.getsize(fout) > 1024:
                    logging.debug('breaking after {} for testing purposes'.format(fout))
                    break
        else:
            # logging.debug('skipping {}, already completed'.format(fin))
            pass
    return True

def parse_mmp(platconf, imm_dir='imm'):
    raw_mmp_dir = os.path.join(platconf.raw_dir, imm_dir, 'mmp')
    if not os.path.exists(raw_mmp_dir):
        return
    parsed_mmp_dir = os.path.join(platconf.parsed_dir, imm_dir, 'mmp') # FIXME this is wrong for global moorings
    safe_makedirs(parsed_mmp_dir)
    TIMETAGS = os.path.join(parsed_mmp_dir, 'TIMETAGS2.TXT')
    if os.path.exists(TIMETAGS):
        timetags = readlines(TIMETAGS)
    else:
        timetags = []
    for fin in sorted(glob(os.path.join(raw_mmp_dir, 'E*.DAT'))):
        if os.path.getsize(fin) == 0:
            logging.warn('zero length data file: {}'.format(fin))
            continue
        fin_basename = os.path.basename(fin)
        c_fin = os.path.join(raw_mmp_dir, re.sub(r'^E',r'C', fin_basename))
        a_fin = os.path.join(raw_mmp_dir, re.sub(r'^E(.*).DAT',r'A\1.DEC', fin_basename))
        if not os.path.exists(c_fin) or not os.path.exists(a_fin):
            continue
        fout_basename = re.sub(r'\.DAT','.TXT', fin_basename)
        fout = os.path.join(parsed_mmp_dir, fout_basename)
        if os.path.exists(fout):
            logging.debug('{} exists'.format(fout))
        else:
            # make timetags
            execute_command([MMP_UNPACK, parsed_mmp_dir, fin, '-t'])
            timetags.append(tail(TIMETAGS)[0])
            # parse data
            def parse_x(sre='', sub=''):
                x_fin = os.path.join(raw_mmp_dir, re.sub(sre, sub, fin_basename))
                if not os.path.exists(x_fin):
                    logging.warn('{} does not exist'.format(x_fin))
                    return False
                execute_command([MMP_UNPACK, parsed_mmp_dir, x_fin], timeout=5)
                return True
            # E data
            if not parse_x(): continue
            # C data
            if not parse_x(r'^E','C'): continue
            # A data
            if not parse_x(r'^E(.*).DAT',r'A\1.DEC'): continue
            # JSON
            jfin_basename = change_extension(fin_basename, 'TXT')
            jfin = os.path.join(parsed_mmp_dir, jfin_basename)
            fout_basename = re.sub(r'^E(.*).DAT', r'P\1.json', fin_basename)
            fout = os.path.join(parsed_mmp_dir, fout_basename)
            args = ['-i', jfin, '-o', fout]
            module = parser_module('mmp_coastal')
            execute_parser(module, args)
    logging.debug('writing TIMETAGS2.TXT')
    writelines(TIMETAGS, timetags)

def render_imm_templates(platconf, inst=None):
    for name in platconf.imm_insts():
        if inst is not None and name != inst:
            continue
        basename = base_inst(name)
        dcl = platconf.inst_dcl('imm')
        yield render_template(platconf, IMM_TEMPLATE, {
            'name': name,
            'basename': basename,
            'dcl': dcl,
        })
        
def run_templates(platform, deploy=None, inst=None):
    platconf = PlatformConfig(platform, deploy)
    if platconf.platform_type == SURFACE_MOORING:
        for template in render_surface_templates(platconf, inst):
            status = execute_template(template)
            if GLOBAL_DEBUG and not status:
                return
        for template in render_imm_templates(platconf, inst):
            status = execute_template(template)
            if GLOBAL_DEBUG and not status:
                return
    elif platconf.platform_type == PROFILER_MOORING:
        templates = render_profiler_templates(platconf)
        for template in templates:
            status = execute_template(template)
            if GLOBAL_DEBUG and not status:
                return
        # now parse mmp
        try:
            parse_mmp(platconf)
        except:
            traceback.print_exc()

if __name__=='__main__':
    import sys
    platform = sys.argv[1]
    try:
        deploy = sys.argv[2]
    except IndexError:
        deploy = None
    try:
        inst = sys.argv[3]
    except IndexError:
        inst = None
    if deploy is None:
        deps = latest_two_deployments(platform)
        for d in deps:
            run_templates(platform, d, inst)
    else:
        run_templates(platform, deploy, inst)
