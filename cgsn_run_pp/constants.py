GLOBAL_DEBUG = False

LOGGING_FORMAT = '%(asctime)-15s %(levelname)s %(message)s'

SURFACE_MOORING = 'SM'
PROFILER_MOORING = 'PM'
GLOBAL_MOORING = 'GM'

PYTHON='python'

# DATA_ROOT = '/home/ooiuser/data'
DATA_ROOT = '/data'
DEPLOY_CONFIG_ROOT = '/home/ooiuser/config/cgsn-dataset_config'

RAW_DATA_ROOT = DATA_ROOT + '/raw'
PARSED_DATA_ROOT = DATA_ROOT + '/proc'
PROCESSED_DATA_ROOT = DATA_ROOT + '/erddap'

CALIBRATION_URL_TEMPLATE = 'https://raw.githubusercontent.com/ooi-integration/asset-management/master/calibration/{}.csv'
