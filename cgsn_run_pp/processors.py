import sys
import re
import importlib
import types

from .utils import module_exists, list_submodules, get_function

PROCESSORS_MODULE = 'cgsn_processing.process'
PROCESSOR_PREFIX = 'proc_'

def processor_module(processor_basename):
    return '{}.{}{}'.format(PROCESSORS_MODULE, PROCESSOR_PREFIX, processor_basename)

def list_processors():
    """list all processor basenames"""
    pbs = []
    for mod_name in list_submodules(PROCESSORS_MODULE):
        regex = '^{}'.format(PROCESSOR_PREFIX)
        if re.match(regex, mod_name):
            processor_basename = re.sub(regex, '', mod_name)
            pbs.append(processor_basename)
    return pbs

def processor_exists(processor):
    return module_exists(processor) or module_exists(processor_module(processor))

def main_function(processor_basename):
    mod_name = processor_module(processor_basename)
    return get_function(mod_name, 'main')

def run_processor(processor_basename, argv=None):
    main_fn = main_function(processor_basename)
    main_fn(argv) # caller's responsibility to handle exceptions

