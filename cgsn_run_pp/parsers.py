import re

from .utils import module_exists, list_submodules

"""
parsers are in the module PARSERS_MODULE
they have names like parse_* where * is the parser's "basename"
"""

PARSERS_MODULE = 'cgsn_parsers.parsers'
PARSER_PREFIX = 'parse_'

def parser_module(parser_basename):
    """given spkir returns cgsn_parsers.parsers.parse_spkir"""
    return '{}.{}{}'.format(PARSERS_MODULE, PARSER_PREFIX, parser_basename)

def list_parsers():
    """list all parser basenames"""
    pbs = []
    # list the submodules of the parsers pkg
    for mod_name in list_submodules(PARSERS_MODULE):
        # is the submodule a parser?
        regex = '^{}'.format(PARSER_PREFIX) # i.e., '^parse_'
        if re.match(regex ,mod_name):
            parser_basename = re.sub(regex,'',mod_name)
            pbs.append(parser_basename)
    return pbs

def parser_exists(parser):
    """parser is either a parser basename or full parser module name,
    return whether that module exists"""
    return module_exists(parser) or module_exists(parser_module(parser))
