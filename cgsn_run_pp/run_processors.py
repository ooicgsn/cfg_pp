import re
import os
from glob import glob

from .platconf import PlatformConfig, base_hotel, latest_two_deployments
from .deployconf import get_deploy_config
from .utils import safe_makedirs, newer_than, change_extension, touch
from .processors import processor_exists, run_processor

import logging
from .constants import LOGGING_FORMAT, GLOBAL_DEBUG
logging.basicConfig(format=LOGGING_FORMAT, level=logging.DEBUG)

PROCESSOR_BY_INST_BASENAME = {
    'fb250': 'syslog_fb250',
    'irid': 'syslog_irid',
    'rda': 'syslog_rda',
    'mmp': 'mmp_coastal',
    '3dmgx3': 'mopak',
# fixme deal with superv
}

def nc_out_for_proc(nc_out, proc_basename):
    """ most processors have an input path foo.json and output path
    foo.nc, but mmp_coastal has foo.json -> foo-{e,a,c}data.nc """
    if proc_basename != 'mmp_coastal':
        return nc_out
    else:
        path, ext = os.path.splitext(nc_out)
        return '{}-edata.nc'.format(path) # use edata as proxy for others

class MultiConfig(object):
    def __init__(self, platconf, deployconf):
        self.platconf = platconf
        self.deployconf = deployconf
    def subassy_depth(self, subassy, path=None):
        default_depth = {
            'mfn': 9999,
            'nsif': 7, # FIXME 12 for global surface moorings
            'buoy': 0,
        }
        if subassy in default_depth:
            return self.deployconf.get_depth(subassy, default_depth[subassy])
        else:
            return 0
    def superv_depth(self, path):
        # this is kludgy. parse the path to find the subassy
        for p in re.split('/',path):
            if p in ['nsif', 'dcl']: # FIXME stc?
                return self.subassy_depth(p)
        return 0
    def inst_depth(self, inst, path=None):
        if inst == 'superv' and path is not None:
            return self.superv_depth(path)
        sa_depth = self.subassy_depth(self.platconf.inst_subassy(inst))
        return self.deployconf.get_depth(inst, sa_depth)
    def lat_lon(self):
        lat, lon = self.deployconf.get_lat_lon()
        lat = self.platconf.lat if lat is None else lat
        lon = self.platconf.lon if lon is None else lon
        return lat, lon
        
def run_all(platform, deploy=None, inst=None):
    platconf = PlatformConfig(platform, deploy)
    deployconf = get_deploy_config(platform, platconf.deploy)
    multiconf = MultiConfig(platconf, deployconf)
    for d, sds, fs in os.walk(platconf.parsed_dir):
        if not sds:
            b = os.path.basename(d)
            if b in platconf.hotels('dcl') + platconf.hotels('cpm'):
                proc_basename = os.path.basename(os.path.dirname(d))
                hotel = b
            else:
                hotel = None
                proc_basename = b
            proc_basename = PROCESSOR_BY_INST_BASENAME.get(proc_basename, proc_basename)
            if proc_basename == 'superv' and hotel is not None:
                proc_basename = 'superv_{}'.format(base_hotel(hotel))
            if not processor_exists(proc_basename):
                logging.debug('no processor for {}'.format(proc_basename))
                continue
            for json_in in list(reversed(sorted(glob(os.path.join(d,'*.json'))))):
                arg_nc_out = change_extension(re.sub(platconf.parsed_dir, platconf.processed_dir, json_in), 'nc')
                nc_out = nc_out_for_proc(arg_nc_out, proc_basename)
                fail_out = nc_out + '.failed'
                if not newer_than(json_in, nc_out):
                    #logging.debug('skipping {}, already completed'.format(json_in))
                    continue
                if os.path.exists(fail_out):
                    continue
                safe_makedirs(os.path.dirname(nc_out))
                inst_name = os.path.basename(json_in).split('.')[1] # e.g., 20170923.spkir.json
                if inst is not None and not inst_name.startswith(inst):
                    break
                try:
                    lat, lon = multiconf.lat_lon()
                    args = [
                        '-i', json_in,
                        '-o', arg_nc_out,
                        '-p', platconf.platform,
                        '-d', platconf.deploy,
                        '-lt', str(lat),
                        '-lg', str(lon),
                        '-dp', str(multiconf.inst_depth(inst_name, json_in)),
                    ]
                    coeff_file = os.path.join(d, '{}.coeff'.format(inst_name))
                    if deployconf.has_serial_number(inst_name):
                        args += ['-c', coeff_file, '-sn', str(deployconf.get_serial_number(inst_name))]
                    elif deployconf.has_calibration(inst_name):
                        args += ['-c', coeff_file, '-u', deployconf.get_calibration_url(inst_name)]
                    if deployconf.has_bin_size(inst_name):
                        args += ['-bs', str(deployconf.get_bin_size(inst_name))]
                    if inst_name.startswith('flort') or inst_name.startswith('nutnr'):
                        ctd = platconf.inst_ctd(inst_name)
                        if ctd:
                            args += ['-df', ctd]
                    logging.debug('{} {}'.format(proc_basename, args)) # FIXME debug
                    # ready to go: run it
                    run_processor(proc_basename, args)
                    # see if we got something
                    if os.path.exists(nc_out):
                        logging.info('{} -> {}'.format(json_in, nc_out))
                        if GLOBAL_DEBUG:
                            break # quit after one successful file
                except KeyboardInterrupt:
                    raise
                except:
                    import traceback
                    traceback.print_exc()
                    touch(fail_out)

if __name__=='__main__':
    import sys
    platform = sys.argv[1]
    try:
        deploy = sys.argv[2]
    except IndexError:
        deploy = None
    try:
        inst = sys.argv[3]
    except IndexError:
        inst = None
    if deploy is None:
        deps = latest_two_deployments(platform)
        for d in deps:
            run_all(platform, d, inst)
    run_all(platform, deploy, inst)
